
------------------------------------------------------------------------
{-
                       __  ____  __                       _
                       \ \/ /  \/  | ___  _ __   __ _  __| |
                        \  /| |\/| |/ _ \| '_ \ / _` |/ _` |
                        /  \| |  | | (_) | | | | (_| | (_| |
                       /_/\_\_|  |_|\___/|_| |_|\__,_|\__,_|
-}

------------------------------------------------------------------------
-- Imports 
--
import XMonad
import XMonad.Actions.Navigation2D as N2D
import XMonad.Actions.WindowNavigation as WN
import XMonad.Actions.CycleWS
import XMonad.Hooks.ManageDocks
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps 
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.NoBorders
import XMonad.Util.SpawnOnce
import XMonad.Hooks.ManageHelpers (isFullscreen,doFullFloat)
import XMonad.Util.Run(spawnPipe,safeSpawn)
import Data.Monoid
import Data.Default
import Data.List (sortBy) --poly
import Data.Function (on) --poly
import Control.Monad (forM_, join) --poly
import Graphics.X11.ExtraTypes.XF86
import XMonad.Util.NamedWindows (getName) --poly
import System.Exit

import XMonad.Layout.Mosaic
import XMonad.Layout.MultiColumns
import XMonad.Layout.Circle

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

------------------------------------------------------------------------
-- Variables 
--
myModMask = mod4Mask -- mod is the key to execute commands
myTerminal = "alacritty" -- terminal var
myBorderWidth = 3 -- Width of the window border in pixels.
gapsSize = 20 -- size of space between tiles
myFocusFollowsMouse = True -- Whether focus follows the mouse pointer
myNormalBorderColor  = "#393638" -- unfocused border color
myFocusedBorderColor = "#485D68" -- focused border color
myWorkspaces = map show [1..5] --workspaces 

------------------------------------------------------------------------
-- Startup commands
--
myStartupHook = do
    spawnOnce "mons -a" -- automatically detects monitors
    spawnOnce "xrandr -s 0 && mons -m" -- change resolution and apply in all monitors
    spawnOnce "nitrogen --set-scaled $HOME/.xmonad/walls/adapta.jpg" --wall
    spawnOnce "start-pulseaudio-x11" -- audioplugin
    spawnOnce "xcape -e 'Super_L=Super_L|R'" -- to super key
    spawnOnce "greenclip" --clipboard
    spawnOnce "numlockx on" -- turn on numerical keyboard
    spawn     "pkill picom; picom " --composer
    spawn     "nm-applet" --network manager
    spawn     "blueman-applet" --bluethoot manager
    spawn     "setxkbmap us intl." --set keyboard layout
    spawn     "xset led 3" --turn on keyboard led
    spawn     "pkill polybar & $HOME/.config/polybar/launch.sh" --polybar

------------------------------------------------------------------------
-- Layouts:
--
gs = gapsSize -- shortening 
myLayout = 
       toggleLayouts full 
     $ gaps [(U, gs + 35),(D,gs), (L,gs), (R,gs)]
     $ spacing gs  
     $     mosaicL
		   ||| multi
     where
       mosaicL = avoidStruts $ mosaic 1 [3,3]
       multi = avoidStruts $ multiCol [1] 3 0.02 0.5
       full = avoidStruts $ noBorders Full
------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
altMask = mod1Mask
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    -- launch a terminal
    [ ((modm,               xK_Return), spawn myTerminal),
    
    -- launch browser
    ((modm,               xK_b     ), spawn "firefox"),

		-- launch station
    ((modm,               xK_s     ), spawn "ferdi"),
    -- launch file manager
    ((modm,               xK_f     ), spawn "pcmanfm"),

    ((modm,               xK_t     ), spawn "xterm"),

    -- launch screenshot
    ((modm .|. shiftMask,   xK_s     ), spawn "flameshot gui"),

    -- launch rofi 
    ((modm,               xK_r     ), spawn "rofi -show drun -columns 2 -show-icons"),

    ((modm,               xK_d     ), spawn "dmenu_run"),

    -- close focused window
    ((modm,               xK_q     ), kill),

    -- Move focus to the next window
    ((modm,               xK_Tab     ), windows W.focusDown),

    -- Move focus to the previous window
    ((modm .|. shiftMask,   xK_Tab    ), windows W.focusUp  ),    

    -- Display settings
    ((altMask,   xK_n     ), spawn "xrandr -s 0 && pkill picom; picom"), -- normal resolution
    ((altMask,   xK_z     ), spawn "xrandr -s 7 && pkill picom; picom"), -- zoom resolution
    ((altMask,   xK_p     ), spawn "mons -o"), -- only pimary monitor
    ((altMask,   xK_s     ), spawn "mons -s"), -- only secondary monitor
    ((altMask,   xK_b     ), spawn "mons -d"), -- duplicates monitors
    ((altMask,   xK_m     ), spawn "mons -m"), -- mirror monitors
    ((altMask,   xK_l     ), spawn "mons -e --rigth"), -- mirror monitors

		-- Volume Control
    ((0,xF86XK_AudioLowerVolume ), spawn "pamixer -d 5" ),
    ((0,xF86XK_AudioRaiseVolume ), spawn "pamixer -i 5" ),
		
		-- Volume Control
    ((0,xF86XK_MonBrightnessDown), spawn "xbacklight -dec 10" ),
    ((0,xF86XK_MonBrightnessUp),   spawn "xbacklight -inc 10" ),


    -- Swap adjacent windows
    ((modm .|. shiftMask,   xK_l     ), windowSwap WN.R False),
    ((modm .|. shiftMask,   xK_h     ), windowSwap WN.L False),
    ((modm .|. shiftMask,   xK_j     ), windowSwap WN.D False),
    ((modm .|. shiftMask,   xK_k     ), windowSwap WN.U False),
        
    -- Rotate through the available layout algorithms
    ((modm,               xK_comma), sendMessage NextLayout),
     
		-- alternate with the full screen layout
    ((modm,  xK_space), sendMessage ToggleLayout),

    -- Push window back into tiling
    ((modm,               xK_t     ), withFocused $ windows . W.sink),
   
    -- move to next or prev desktop
    ((modm,               xK_bracketright),    nextWS),
    ((modm,               xK_bracketleft ),    prevWS),
    
    -- Restart xmonad
    ((modm .|. shiftMask, xK_q     ), spawn "xmonad --recompile; xmonad --restart")
    ]
    ++
    
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    [((m .|. modm, k), windows $ f i)
    | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_5]
    , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


------------------------------------------------------------------------
-- Window rules:
--
myManageHook = composeAll
    [ className =? "polybar"    --> doIgnore,
      resource  =? "polybar"    --> doIgnore,
      className =? "tint2"      --> doIgnore,
      resource  =? "tint2"      --> doIgnore,
      className =? "teamviewer" --> doIgnore,
      resource  =? "teamviewer" --> doIgnore,
      className =? "openboard"  --> doIgnore,
      resource  =? "openboard"  --> doIgnore,
      className =? "virtualbox" --> doIgnore,
      resource  =? "virtualbox" --> doIgnore

    ]

------------------------------------------------------------------------
-- Event handling

-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
-- * NOTE: EwmhDesktops users should use the 'ewmh' function from
-- XMonad.Hooks.EwmhDesktops to modify their defaultConfig as a whole.
-- It will add EWMH event handling to your custom event hooks by
-- combining them with ewmhDesktopsEventHook.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
--
-- * NOTE: EwmhDesktops users should use the 'ewmh' function from
-- XMonad.Hooks.EwmhDesktops to modify their defaultConfig as a whole.
-- It will add EWMH logHook actions to your custom log hook by
-- combining it with ewmhDesktopsLogHook.
--
myLogHook = do
  winset <- gets windowset
  title <- maybe (return "") (fmap show . getName) . W.peek $ winset
  let currWs = W.currentTag winset
  let wss = map W.tag $ W.workspaces winset
  let wsStr = join $ map (fmt currWs) $ sort' wss

  io $ appendFile "/tmp/.xmonad-title-log" (title ++ "\n")
  io $ appendFile "/tmp/.xmonad-workspace-log" (wsStr ++ "\n")

  where fmt currWs ws
          | currWs == ws = "[" ++ ws ++ "]"
          | otherwise    = " " ++ ws ++ " "
        sort' = sortBy (compare `on` (!! 0))

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--

main = do
    forM_ [".xmonad-workspace-log", ".xmonad-title-log"] $ \file -> do
    	safeSpawn "mkfifo" ["/tmp/" ++ file]
    config <- withWindowNavigation (xK_k, xK_h, xK_j, xK_l)
    	$ def{
    -- simple stuff
    terminal           = myTerminal,
    focusFollowsMouse  = myFocusFollowsMouse,
    borderWidth        = myBorderWidth,
    modMask            = myModMask,
    workspaces         = myWorkspaces,
    normalBorderColor  = myNormalBorderColor,
    focusedBorderColor = myFocusedBorderColor,

    -- key bindings
    keys               = myKeys,
    mouseBindings      = myMouseBindings,

    -- hooks, layouts
    layoutHook         = myLayout,
    logHook            = myLogHook,
    manageHook         = ( isFullscreen --> doFloat ) <+> myManageHook,
    handleEventHook    = myEventHook,
    startupHook        = myStartupHook
    }
    xmonad config 
