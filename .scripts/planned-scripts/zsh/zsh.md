profile
zprofile
git clone https://github.com/ohmyzsh/ohmyzsh.git
cd ohmyzsh
ZSH="$HOME/.config/zsh/" sh install.sh
cp dotfiles zsh files to .config/zsh
rm -r ohmyzsh
