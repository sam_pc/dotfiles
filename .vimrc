
" plugins ------------------

call plug#begin('~/.local/share/nvim/plugged')
  Plug 'rafi/awesome-vim-colorschemes' "theme collection
  Plug 'scrooloose/nerdtree' "arbol de archivos
  Plug 'tiagofumo/vim-nerdtree-syntax-highlight' "nerdtree colorines
  Plug 'ryanoasis/vim-devicons' "nerdtree icons
  Plug 'vim-airline/vim-airline' "barra airline
  Plug 'vim-airline/vim-airline-themes' "temas para airline
  Plug 'neoclide/coc.nvim', {'branch': 'release'} "vscodium plugins
  " Plug 'ervandew/supertab' "navegar en doplete con tab
  Plug 'Shougo/echodoc.vim' "estructura de funcion
  Plug 'sheerun/vim-polyglot' "resalta sintaxys
  Plug 'neovimhaskell/nvim-hs.vim' "haskell support
  Plug 'haya14busa/incsearch.vim' "mejora busqueda
  Plug 'tpope/vim-surround' "facilita cambio de agrupacion
  Plug 'preservim/nerdcommenter' "facilita agregar comentarios
  Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' } "latex preview
  Plug 'tc50cal/vim-terminal' "terminal
  Plug 'skywind3000/vim-terminal-help'
  Plug 'easymotion/vim-easymotion' "movimientos fáciles
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } "buscar cohincidencias
  Plug 'junegunn/fzf.vim' "buscar en los documentos
  Plug 'epilande/vim-react-snippets' "reactsnipets
  Plug 'joaohkfaria/vim-jest-snippets'
  Plug 'neoclide/jsonc.vim', "json with comments support
  Plug 'Honza/vim-snippets'
  " Plug 'SirVer/ultisnips' "gestor de snippets
call plug#end()



"plugins config ------------

"nerdcommenter

  " Agregar un espacio después del delimitador del comentario
  let g:NERDSpaceDelims = 1  

  " Quitar espacios al quitar comentario
  let g:NERDTrimTrailingWhitespace = 1  


  " Quitar resaltado luego de buscar
  let g:incsearch#auto_nohlsearch = 1

"echodoc
  let g:echodoc_enable_at_startup = 1

"ultisnips
  let g:UltiSnipsExpandTrigger="<enter>"
"supertab
  " let g:SuperTabDefaultCompletionType = '<tab>'

"deoplete
  " let g:deoplete#enable_at_startup = 1

"coc
let g:coc_global_extensions = [
  \ 'coc-lists'
	\, 'coc-highlight'
	\, 'coc-html'
	\, 'coc-emmet'
	\, 'coc-css'
	\, 'coc-json'
	\, 'coc-tsserver'
	\, 'coc-vetur'
	\, 'coc-pairs'
	\, 'coc-prettier'
	\, 'coc-python'
	\, 'coc-texlab'
	\, 'coc-pairs'
	\, 'coc-angular'
	\, 'coc-jest'
	\, 'coc-snippets'
	\, 'coc-neosnippet'
	\, 'coc-ultisnips'
	\, 'coc-eslint'
\]

autocmd BufRead,BufNewFile tsconfig.json set filetype=jsonc
command! -nargs=0 Eslint :CocCommand eslint.executeAutofix
command! -nargs=0 Prettier :CocCommand prettier.formatFile

"nerdtree

  " Cambia el directorio actual al nodo padre actual
  let g:NERDTreeChDirMode = 2  
  let g:NERDTreeShowHidden = 1  
  " let g:NERDTreeQuitOnOpen=1


"airline

  " Mostrar buffers abiertos (como pestañas)
  let g:airline#extensions#tabline#enabled = 1 
  let g:airline_theme='hybrid' 

  " Mostrar sólo el nombre del archivo
  let g:airline#extensions#tabline#fnamemod = ':t' 

  " Cargar fuente Powerline y símbolos (ver nota)
  let g:airline_powerline_fonts = 1


"icons

	" adding to vim-airline's tabline
  let g:webdevicons_enable_airline_tabline = 1
	" adding to vim-airline's statusline
  let g:webdevicons_enable_airline_statusline = 1
	" whether or not to show the nerdtree brackets around flags
  let g:webdevicons_conceal_nerdtree_brackets = 1

" vim-terminal
  let g:terminal_cwd= 2

" vim config -----------------
set nu
set termguicolors "enable true colors
set background=dark
colorscheme hybrid_material 
set shiftwidth=2
set mouse=a
set clipboard=unnamed
set noshowmode  " No mostrar el modo actual (ya lo muestra la barra de estado)
set sw=2
set encoding=UTF-8
set showcmd
set relativenumber
" set foldmethod=syntax

                                         
" let g:livepreview_previewer = 'zathura'


" keymaps --------------------
  let mapleader = "," "special key

  "open-terminal
  let g:terminal_key = "<Leader>="

  "Enter pa seleccionar
  " inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : \<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
 

  " Abrir/cerrar con F2
  map  <Leader>f :NERDTreeToggle<CR>
  nmap f         <Plug>(easymotion-s2)

  "easy search
  map /  <Plug>(incsearch-forward)  
  map ?  <Plug>(incsearch-backward) 

  noremap <Leader>k <C-w>k
  noremap <Leader>j <C-w>j
  noremap <Leader>l <C-w>l
  noremap <Leader>h <C-w>h

"   function! s:check_back_space() abort
    " let col = col('.') - 1
    " return !col || getline('.')[col - 1]  =~ '\s'
  " endfunction

 " inoremap <silent><expr> <Tab>
      " \ pumvisible() ? "\<C-n>" :
      " \ <SID>check_back_space() ? "\<Tab>" :
      " \ coc#refresh()

 " inoremap <silent><expr> <NUL> coc#refresh()
 inoremap <expr> <S-j> pumvisible() ? "\<C-n>" : "\<S-j>"
 inoremap <expr> <S-k> pumvisible() ? "\<C-p>" : "\<S-k>"
 inoremap <expr> <CR> pumvisible() ? "\<tab>" : "\<CR>"
 " inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
 " autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
 "

inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'
