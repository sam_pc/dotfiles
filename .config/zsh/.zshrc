#
# Config ------------------------------
#

# set omzsh route
export ZSH="$HOME/.config/zsh/.oh-my-zsh" 

# set personalized config files
ZSH_CUSTOM="$HOME/.config/zsh/custom"

# bind frameworks and plugin calls
source $ZSH_CUSTOM/plugins/plugins.zsh
source $ZSH/oh-my-zsh.sh

# welcome message
pfetch
