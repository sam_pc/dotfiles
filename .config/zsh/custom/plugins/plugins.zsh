#
# Plugins ------------------------------
#
plugins=(
  git
  vi-mode
  zsh-syntax-highlighting
  zsh-completions
  zsh-history-substring-search
)
