#
# alias ------------------------------
# 

# goto configs, files, projects
alias go_xmonad='nvim $HOME/.xmonad/xmonad.hs'
alias go_xmonadv='code $HOME/.xmonad/xmonad.hs'

alias go_zshconfig='nvim $HOME/.config/zsh/.zshrc'
alias go_zshcustom='cd $HOME/.config/zsh/custom/; ls -l'
alias go_zshalias='nvim $HOME/.config/zsh/custom/aliases.zsh'
alias go_zshoptions='nvim $HOME/.config/zsh/custom/options.zsh'
alias go_zshplugins='nvim $HOME/.config/zsh/custom/plugins/plugins.zsh'
alias go_alacrittyconf="nvim $HOME/.config/alacritty/alacritty.yml"
alias go_tareas="nvim $HOME/Documents/umg/tareas.md"
alias go_pla="nvim $HOME/Documents/desktop/sino.md"
alias go_vimconfig='nvim $HOME/.vimrc'

# workspaces
alias go_pgm="cd $HOME/Documents/pgm/; l"
alias go_scripts="cd $HOME/.scripts/; l"
alias go_usb='cd /run/media/$USER; lsd -lh'
alias go_9no="cd $HOME/Documents/umg/9no/; l"
alias go_tesis="cd $HOME/Documents/umg/9no/proyGraduación/tesis; v"
alias go_vue="cd $HOME/Documents/pgm/js/vue/; l"
alias go_projects="cd $HOME/Documents/pgm/projects/; l"
alias go_wrk="cd $HOME/Documents/wrk/; l"
alias go_angular="cd $HOME/Documents/pgm/js/angular/; lsd"
alias go_react="cd $HOME/Documents/pgm/js/react/; lsd"
alias go_slides="cd $HOME/Documents/md/slides/; lsd"
alias go_u="cd $HOME/Documents/umg/; l"

# wrappers
alias v='nvim' 
alias e='nvim' 
alias l='lsd' 
alias ll='lsd -lha' 
alias lt='lsd --tree' 
alias lt2='lsd --tree --depth=2' 
alias dot='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias tty-clock='tty-clock -ct'
alias pcm='pcmanfm .'
alias code='codium'
alias calculator='speedcrunch &'
alias dk="docker"
alias youtube-mp3="youtube-dl --extract-audio --audio-format mp3"
alias ppm="pipenv"
alias set_git_u="git config user.name 'sam_pc'; git config user.email 'spadillac1@miumg.edu.gt'"
alias set_git_personal="git config user.name 'sam_pc'; git config user.email 'sscambara@gmail.com'"
alias set_git_wrk="git config user.name 'sam_pc'; git config user.email 'samuel.padilla@blautech.us'"
alias set_git_tg="git config user.name 'sam_pc'; git config user.email 'spadillac@tigo.com.gt'"
alias cra='create-react-app'
alias atty='alacritty &'
alias grenv="git restore --staged .env.development"

# starters
alias start_vpn_tg="sudo /opt/cisco/anyconnect/bin/vpnagentd"
alias start_horus="start_vpn_tg; go_wrk; cd horus_client; atty; npm run serve"
alias start_boc="go_wrk; cd boc/webviews-boc/; atty; yarn start:backend;"
alias start_boc-production="ssh -i $HOME/Documents/wrk/boc/boc_acces/PRODUCCION/SRV-BOVEDA-CATALOGOS.pem linux@119.8.0.50";
alias start_db_tlf="ssh -i $HOME/Documents/wrk/boc/boc_acces/KEY-DB-BOC-DEV.pem root@119.8.0.130"

#Show
alias show_ip="dig +short myip.opendns.com @resolver1.opendns.com"

# scripts
alias term-clock='ruby /home/sam/.scripts/term-clock/term-clock.rb'
alias weather='curl wttr.in/Jutiapa,Guatemala'
